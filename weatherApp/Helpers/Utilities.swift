//
//  Utilities.swift
//  weatherApp
//
//  Created by Владислав Игнатьев on 10.02.2018.
//  Copyright © 2018 Владислав Игнатьев. All rights reserved.
//

import UIKit


public func configured<Type: AnyObject>(object obj: Type, closure: (_ object: Type) -> Void) -> Type {
  /// For reference-type objects (class instances) only. Value-types are not supported
  closure(obj)
  return obj
}


public protocol StoryboardInstantiatable: class {
  /* Условия работы фичи: storyboardId в Storyboard должен быть названием класса */
  static var storyboardId: String { get }
  static var storyboardName: String { get }
}
extension StoryboardInstantiatable where Self: UIViewController {
  public static var storyboardId: String {
    return String(describing: self)
  }

  public static func instantiateFromStoryboard(inBundle storyboardBundle: Bundle = Bundle.main) -> Self {
    let identifier = self.storyboardId
    let storyboard = UIStoryboard(name: storyboardName, bundle: storyboardBundle)

    let viewController = storyboard.instantiateViewController(withIdentifier: identifier) as! Self
    return viewController
  }
}

enum StoryboardNamed: CustomStringConvertible {
  case main

  var description: String {
    switch self {
    case .main: return "Main"
    }
  }
}


extension UIViewController {
  @objc func showOkAlertWith(message: String, completion: (() -> Void)? = nil) {
    showOkAlertWith(title: nil, message: message, completion: completion)
  }
  func showOkAlertWith(title: String?, message: String?, completion: (() -> Void)? = nil) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
      completion?()
    }
    alert.addAction(okAction)

    present(alert, animated: true, completion: nil)
  }
}


struct JsonMappingError: Error {
  private let description: String

  init(key: String, type: Any.Type) {
    description = "Unable to map value, key: \(key), valueType: \(type)"
  }

  init(description: String) {
    self.description = description
  }

  var localizedDescription: String {
    return description
  }
}