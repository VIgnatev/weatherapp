//
//  TableViewRegisterable
//  weatherApp
//
//  Created by Владислав Игнатьев on 02.02.2018.
//  Copyright © 2018 Владислав Игнатьев.All rights reserved.
//

import UIKit


/* Для строк */

/// Defines something that can be dequeued from a table view using a reuseIdentifier
/// A cell that is defined in a storyboard should implement this.
public protocol TableViewDequeuable: class {
  static var reuseIdentifier: String { get }
}

extension TableViewDequeuable {
  /// Default implementation of reuseIndentifier is to use the class name,
  /// this can be specifically implemented for difference behaviour
  public static var reuseIdentifier: String {
    return String(describing: self)
  }
}


/// Defines something that can be registered with a table view, using the reuseIdentifer
/// A cell that is laid out programmically or in a nib (that also implements NibLoadable) should implement this
public protocol TableViewRegisterable: TableViewDequeuable {}

public extension TableViewRegisterable where Self: UITableViewHeaderFooterView {
  var reuseIdentifier: String? {
    return Self.reuseIdentifier
  }
}



// Load Cell from Nib
public protocol NibLoadable {
  /**
   Условия для работы фичи:
   – xib файл должен называться как класс
   
   При написании тестов передавать bundle в аргумент методов loadFromNib(inBundle:) и nib(inBundle:)
   При разработке указывать bundle не нужно, т.к поставляется дефолтный
   */
  
  static var nibName: String { get }
  static var defaultNibBundle: Bundle { get }
  
  static func nib(fromBundle nibBundle: Bundle) -> UINib
  static func loadFromNib(inBundle nibBundle: Bundle) -> Self
}

public extension NibLoadable where Self: UIView {
  static var nibName: String {
    return String(describing: self)
  }
  
  static var defaultNibBundle: Bundle {
    return Bundle.main
  }
  
  static func nib(fromBundle nibBundle: Bundle = Self.defaultNibBundle) -> UINib {
    return UINib(nibName: nibName, bundle: nibBundle)
  }
  
  static func loadFromNib(inBundle nibBundle: Bundle = Self.defaultNibBundle) -> Self {
    let view = nib(fromBundle: nibBundle).instantiate(withOwner: self, options: nil).first as! Self
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }
}

public extension NibLoadable where Self: UIView {
  static func loadFrom(cachedNib nib: UINib) -> Self {
    /*
     На случай, когда одну view нужно много раз загружать. Можно ее nib сохранить и передавтаь в этот метод
     Позволяет немного ускорить процесс по сравнению с loadFromNib()
     */
    let view = nib.instantiate(withOwner: self, options: nil).first as! Self
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
  }
}


/* Для TableView */

public extension UITableView {
  
  func dequeue<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T where T: TableViewDequeuable {
    guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
      fatalError("Could not dequeue cell with identifier \(T.reuseIdentifier)")
    }
    return cell
  }
  
  func dequeue<T: UITableViewHeaderFooterView> () -> T? where T: TableViewDequeuable {
    return dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T
  }
  
  func register<T: UITableViewCell>(_ cellType: T.Type) where T: TableViewRegisterable {
    register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
  }
  
  func register<T: UITableViewCell>(_ cellType: T.Type) where T: TableViewRegisterable, T: NibLoadable {
    register(T.nib(), forCellReuseIdentifier: T.reuseIdentifier)
  }
  
  func register<T: UITableViewHeaderFooterView>(_ headerFooterType: T.Type) where T: TableViewRegisterable {
    register(T.self, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
  }
  
  func register<T: UITableViewHeaderFooterView>(_ headerFooterType: T.Type) where T: TableViewRegisterable, T: NibLoadable {
    register(T.nib(), forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
  }
}



public extension UICollectionView {
  func dequeueCollectionCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T where T: TableViewDequeuable {
    guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
      fatalError("Could not dequeue cell with identifier \(T.reuseIdentifier)")
    }
    return cell
  }
}
