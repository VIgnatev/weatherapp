//
//  CityList.swift
//  weatherApp
//
//  Created by Владислав Игнатьев on 11.02.2018.
//  Copyright © 2018 Владислав Игнатьев. All rights reserved.
//

import Foundation

// MARK: - Country
struct Country {
  
  // MARK: Properties
  let name: String
  let cities: [City]
  
  
  // MARK: - Initializer
  init(name: String, cities: [City]) {
    self.name = name
    self.cities = cities
  }
}

// MARK: - City
struct City {
  
  // MARK: Properties
  let name: String
  
  
  // MARK: - Initializer
  init(name: String) {
    self.name = name
  }
}

// MARK: Equatable for City
extension City: Equatable {
  
  // MARK: Methods
  static func ==(lhs: City, rhs: City) -> Bool {
    return lhs.name == rhs.name
  }
}
