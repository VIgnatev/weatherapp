//
//  City.swift
//  weatherApp
//
//  Created by Владислав Игнатьев on 02.02.2018.
//  Copyright © 2018 Владислав Игнатьев. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper


// MARK: - CityEntity
final class CityEntity: Object {
  
  // MARK: Realm properties
  @objc dynamic var name: String = ""
  var myCitiesWeather = List<WeatherRealmEntity>()
  
  
  // MARK: - Overrided
  override static func primaryKey() -> String? {
    return "name"
  }
}


// MARK: - CityWeatherInfoList
struct CityWeatherInfoList: ImmutableMappable {
  
  // MARK: Properties
  let cityWeatherInfoList: [CityWeatherInfo]
  
  
  // MARK: Initializer
  init(map: Map) throws {
    self.cityWeatherInfoList = try map.value("list")
  }
}
