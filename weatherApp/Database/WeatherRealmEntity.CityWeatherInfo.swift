//
//  Weather.swift
//  weatherApp
//
//  Created by Владислав Игнатьев on 02.02.2018.
//  Copyright © 2018 Владислав Игнатьев.All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper


// MARK: - WeatherRealmEntity
final class WeatherRealmEntity: Object {
  
  // MARK: Realm properties
  @objc dynamic var dateInterval: Double         = 0.0
  @objc dynamic var temperature: Double          = 0.0
  @objc dynamic var pressure: Double             = 0.0
  @objc dynamic var humidity: Int                = 0
  @objc dynamic var weatherName: String          = ""
  @objc dynamic var weatherIconImageName: String = ""
  @objc dynamic var windSpeed: Double            = 0.0
  @objc dynamic var windDegrees: Double          = 0.0
  @objc dynamic var cityName: String             = ""
  
  
  // MARK: - Methods
  func fillWith(_ object: CityWeatherInfo, forCity city: String) {
    self.dateInterval         = object.dateInterval
    self.temperature          = object.temperature
    self.pressure             = object.pressure
    self.humidity             = object.humidity
    self.weatherName          = object.details.description
    self.weatherIconImageName = object.details.imageName
    self.windSpeed            = object.windSpeed
    self.windDegrees          = object.windDegrees
    self.cityName             = city
  }
  
  //  func buildCityWeatherInfo() -> CityWeatherInfo {
  //  }
}


// MARK: - CityWeatherInfo
struct CityWeatherInfo: ImmutableMappable {
  
  // MARK: Properties
  let dateInterval: Double
  let temperature : Double
  let pressure    : Double
  let humidity    : Int
  let details     : Details
  let windSpeed   : Double
  let windDegrees : Double
  
  // MARK: Computed properties
  var date: Date {
    return Date(timeIntervalSince1970: dateInterval)
  }
  
  
  // MARK: - Initializer
  init(map: Map) throws {
    self.dateInterval           = try map.value("dt")
    self.temperature            = try map.value("main.temp")
    self.pressure               = try map.value("main.pressure")
    self.humidity               = try map.value("main.humidity")
    self.windSpeed              = try map.value("wind.speed")
    self.windDegrees            = try map.value("wind.deg")
    
    let detailsArray: [Details] = try map.value("weather") // В массиве должен приходить только 1 объект
    assert(detailsArray.count == 1, "Incorrect count of weatherDetails")
    
    guard let singularDetails = detailsArray.first else {
      throw JsonMappingError(description: "No weather details")
    }
    
    self.details = singularDetails
  }
  
  
  // MARK: - Nested struct
  struct Details: ImmutableMappable {
    
    // MARK: Properties
    let id         : Int
    let main       : String
    let description: String
    let imageName  : String
    
    
    // MARK: - Initilizer
    init(map: Map) throws {
      self.id          = try map.value("id")
      self.main        = try map.value("main")
      self.description = try map.value("description")
      self.imageName   = try map.value("icon")
    }
  }
}
