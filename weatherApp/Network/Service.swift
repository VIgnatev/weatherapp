//
//  Service.swift
//  weatherApp
//
//  Created by Владислав Игнатьев on 02.02.2018.
//  Copyright © 2018 Владислав Игнатьев. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper


// MARK: - WeatherService
final class Service {
  
  // MARK: Properties
  private let baseUrl = "http://api.openweathermap.org"
  private let apiKey  = "f1d90ed4dedcb32712769f42105f226d"
  
  
  // MARK: - Methods
  func loadWeatherData(forCityName cityName: String,
                       completion: @escaping (_ result: Result<CityWeatherInfoList>) -> Void) {
    
    let path = "/data/2.5/forecast"
    let url  = baseUrl + path
    
    let parameters: Parameters = [
      "q"     : cityName,
      "units" : "metric",
      "appid" : apiKey
    ]
    
    Alamofire.request(url, method: .get, parameters: parameters).validate().responseJSON { response in
      switch response.result {
      case .success(let value):
        guard let weatherInfoList = try? Mapper<CityWeatherInfoList>().map(JSONObject: value) else {
          let error = JsonMappingError(description: "Weather Mapping error")
          completion(.failure(error))
          
          return
        }
        
        completion(.success(weatherInfoList))
        
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }
  
  func getCountries() -> Result<[Country]> {
    
    let outputResult: Result<[Country]>
    
    if let path = Bundle.main.path(forResource: "countries", ofType: "json") {
      do {
        let rawData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        
        guard let rawJson = try? JSONSerialization.jsonObject(with: rawData, options: []),
          let rawDict = rawJson as? [String : [String]] else {
            
            let error = JsonMappingError(description: "Не удалось загрузить список стран")
            outputResult = Result.failure(error)
            return outputResult
        }
        
        var countries = [Country]()
        
        for (countryName, cityNamesArray) in rawDict {
          let citiesArray: [City] = cityNamesArray.map { City(name: $0) }
          let country = Country(name: countryName, cities: citiesArray)
          
          countries.append(country)
        }
        
        outputResult = Result.success(countries)
        
      } catch {
        outputResult = Result.failure(error)
      }
      
    } else {
      let error = JsonMappingError(description: "Не удалось загрузить список стран")
      outputResult = Result.failure(error)
    }
    
    return outputResult
  }
  
  // Don't work
  private func saveWeatherData(_ weather: [WeatherRealmEntity], for city: String) {
//    do {
//      let realm = try! Realm()
//
//      guard let city = realm.object(ofType: CityEntity.self, forPrimaryKey: city) else { return }
//      let oldWeather = city.myCitiesWeather
//
//      realm.beginWrite()
//      realm.delete(oldWeather)
//      city.myCitiesWeather.append(objectsIn: weather)
//
//      try realm.commitWrite()
//    } catch {
//      print(error)
//    }
  }
}
