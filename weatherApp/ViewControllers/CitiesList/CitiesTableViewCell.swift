//
//  CitiesTableViewCell.swift
//  weatherApp
//
//  Created by Владислав Игнатьев on 11.02.2018.
//  Copyright © 2018 Владислав Игнатьев. All rights reserved.
//

import UIKit


// MARK: - CitiesTableViewCell
final class CitiesTableViewCell: UITableViewCell, TableViewDequeuable {
  
  // MARK: IBOutlets
  @IBOutlet private weak var cityNameLabel: UILabel!
  
  
  // MARK: - Overrided
  override func prepareForReuse() {
    super.prepareForReuse()
    
    setCityNameLabel(text: nil)
  }
  
  
  // MARK: - Methods
  func setCityNameLabel(text labelText: String?) {
    self.cityNameLabel.text = labelText
  }
}
