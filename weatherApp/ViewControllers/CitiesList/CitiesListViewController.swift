//
//  CitiesListViewController.swift
//  weatherApp
//
//  Created by Владислав Игнатьев on 10.02.2018.
//  Copyright © 2018 Владислав Игнатьев. All rights reserved.
//

import UIKit


// MARK: - CitiesListViewController
final class CitiesListViewController: UIViewController {
  
  // MARK: - Properties
  private var allCountries    = [Country]()
  
  private var allCities       = [City]()    // .count == 83840
  private var tableViewCities = [City]()
  
  // MARK: IBOutlets
  @IBOutlet private weak var tableView: UITableView!
  @IBOutlet private weak var searchBar: UISearchBar!
  
  // MARK: Closure
  private var citySelectionAction = { (_ city: City) -> Void in }
  
  
  // MARK: - Overrided
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "All Cities"
    
    searchBar.delegate      = self
    searchBar.returnKeyType = .done
    
    tableView.dataSource = self
    tableView.delegate   = self
    
    let countriesResult = Service().getCountries()
    
    switch countriesResult {
    case .success(let allCountries):
      
      DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async { [weak self] in
        // Список всех городов получаем в background потоке
        let allCitiesArray = allCountries.map { $0.cities }.flatMap { $0 }
        guard let sself = self else { return }
        
        sself.allCities = allCitiesArray
        sself.tableViewCities = sself.getCitiesToShow(from: allCitiesArray)
        
        DispatchQueue.main.async { // Обновляем UI
          self?.tableView?.reloadData()
        }
      }
      
    case .failure(let error):
      showOkAlertWith(title: "Ошибка", message: error.localizedDescription)
    }
  }
  
  
  // MARK: - Methods
  func getCitiesToShow(from cities: [City]) -> [City] {
    // Показываем только 50 городов, сортированных по алфавиту
    let citiesToShow = Array(cities.prefix(50).sorted { $0.name < $1.name })
    return citiesToShow
  }
  
  func configureSelectionAction(closure: @escaping (_ city: City) -> Void ) {
    self.citySelectionAction = closure
  }
}



// MARK: - TableViewDataSource
extension CitiesListViewController: UITableViewDataSource {
  
  // MARK: - Methods
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return tableViewCities.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: CitiesTableViewCell = tableView.dequeue(forIndexPath: indexPath)
    
    let city = tableViewCities[indexPath.row]
    
    cell.setCityNameLabel(text: city.name)
    
    return cell
  }
}


// MARK: - TableViewDelegate
extension CitiesListViewController: UITableViewDelegate {
  
  // MARK: - Methods
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let selectedCity = tableViewCities[indexPath.row]
    citySelectionAction(selectedCity)
    
    self.navigationController?.popViewController(animated: true)
  }
}


// MARK: - SearchBarDelegate
extension CitiesListViewController: UISearchBarDelegate {
  
  // MARK: - Methods
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    
    // Если SearchBar пустой присваиваем allCities, иначе результаты поиска
    if searchText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
      tableViewCities = allCities
    } else {
      let allCitiesCopy = allCities
      
      // Асинхронно выполняем тяжелые операции в глобальной очереди
      DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async { [weak self] in
        let filteredCities = allCitiesCopy.filter { $0.name.contains(searchText.capitalized) }
        guard let sself = self else { return }
        sself.tableViewCities = sself.getCitiesToShow(from: filteredCities)
        
        DispatchQueue.main.async { // Обновляем UI
          self?.tableView?.reloadData()
        }
      }
    }
  }
}


// MARK: StoryboardInstantiatable
extension CitiesListViewController: StoryboardInstantiatable {
  
  // MARK: Computed properties
  static var storyboardName: String {
    return StoryboardNamed.main.description
  }
}
