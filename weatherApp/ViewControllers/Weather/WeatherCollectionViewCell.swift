//
//  WeatherCollectionViewCell.swift
//  weatherApp
//
//  Created by Владислав Игнатьев on 01.02.2018.
//  Copyright © 2018 Владислав Игнатьев. All rights reserved.
//

import UIKit


// MARK: - WeatherCollectionViewCell
final class WeatherCollectionViewCell: UICollectionViewCell, TableViewDequeuable {
  
  // MARK: IBOutlets
  @IBOutlet private weak var temperatureLabel: UILabel!
  @IBOutlet private weak var icon            : UIImageView!
  @IBOutlet private weak var dateLabel       : UILabel!
  
  
  // MARK: - Overrided
  override func prepareForReuse() {
    super.prepareForReuse()
    
    setTemperatureLabel(text: nil)
    setIcon(image: nil)
    setDateLabel(text: nil)
  }
  
  
  // MARK: - Methods
  func setTemperatureLabel(text temperature: String?) {
    temperatureLabel.text = temperature
  }
  func setIcon(image icon: UIImage?) {
    self.icon.image = icon
  }
  func setDateLabel(text date: String?) {
    dateLabel.text = date
  }
}
