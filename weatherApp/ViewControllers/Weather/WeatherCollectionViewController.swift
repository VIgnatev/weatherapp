//
//  WeatherCollectionViewController.swift
//  weatherApp
//
//  Created by Владислав Игнатьев on 01.02.2018.
//  Copyright © 2018 Владислав Игнатьев. All rights reserved.
//

import UIKit
import RealmSwift

// MARK: - WeatherCollectionViewController
final class WeatherCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
  
  // MARK: - Properties
  var cityName: String = ""
  
  // MARK: Private properties
  private var dataSource     = [CityWeatherInfo]()
  private let weatherService = Service()
  private let calendar       = NSCalendar.current
  
  private let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
  
  // MARK: Computed properties
  private let dateFormatter: DateFormatter = {
    let dateFormatter        = DateFormatter()
    dateFormatter.dateFormat = "EEEE d,"
    
    return dateFormatter
  }()
  
  private let timeFormatter: DateFormatter = {
    let timeFormatter        = DateFormatter()
    timeFormatter.dateFormat = "H:mm"
    
    return timeFormatter
  }()
  
  
  // MARK: - Overrided
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "\(cityName) Forecast"
    
    self.view.layer.cornerRadius  = 3.0
    self.view.layer.masksToBounds = true
    
    do { // Настраиваем отображение CollectionView
      let width = UIScreen.main.bounds.width
      layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
      layout.itemSize     = CGSize(width: width, height: width / 2.5)
      layout.minimumInteritemSpacing = 0
      layout.minimumLineSpacing      = 0
      collectionView!.collectionViewLayout = layout
    }
    
    weatherService.loadWeatherData(forCityName: cityName) { result in
      
      switch result {
      case .success(let weatherInfoList):
        self.dataSource = weatherInfoList.cityWeatherInfoList
        
        DispatchQueue.main.async {
          self.collectionView?.reloadData()
        }
        
      case .failure(_):
        self.showOkAlertWith(title: "Network error", message: "Check network connection.")
      }
    }
  }
  
  
  // MARK: - Methods
  private func saveForecastToRealm(forecast: [CityWeatherInfo]) {
//    // [CityWeatherInfo] -> [WeatherRealmEntity]
//    for some in forecast {
//      WeatherRealmEntity().fillWith(some, forCity: cityName)
//    }
//
//    do {
//      let realm = try! Realm()
//      print(realm.configuration.fileURL!)
//
//      guard let city = realm.object(ofType: CityEntity.self, forPrimaryKey: cityName) else { return }
//      realm.beginWrite()
//
////      city.myCitiesWeather.append(<#T##object: WeatherRealmEntity##WeatherRealmEntity#>) // [WeatherRealmEntity]
//
//      try realm.commitWrite()
//    } catch {
//      print(error.localizedDescription)
//    }
}

//  func showActivityIndicator() { }
//  func hideActivityIndicator() { }
}


// MARK: - CollectionViewDataSource
extension WeatherCollectionViewController {
  
  // MARK: - Overrided
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return dataSource.count
  }
  
  override func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell: WeatherCollectionViewCell = collectionView.dequeueCollectionCell(forIndexPath: indexPath)
    let weatherInfo = dataSource[indexPath.row]
    
    let weatherDate = dateFormatter.string(from: weatherInfo.date)
    let weatherTime = timeFormatter.string(from: weatherInfo.date)
    
    if calendar.isDateInToday(weatherInfo.date) {
      cell.setDateLabel(text: "Today\n\(weatherTime)")
    } else if calendar.isDateInTomorrow(weatherInfo.date) {
      cell.setDateLabel(text: "Tomorrow\n\(weatherTime)")
    } else {
      cell.setDateLabel(text: weatherDate + "\n\(weatherTime)")
    }
    
    let temperature = Int(round(weatherInfo.temperature))
    cell.setTemperatureLabel(text: String(temperature) + " °C")
    
    let weatherImage = UIImage(named: weatherInfo.details.imageName)
    cell.setIcon(image: weatherImage)
    
    return cell
  }
}


// MARK: - CollectionViewDelegate
extension WeatherCollectionViewController {  }
