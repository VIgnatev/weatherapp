//
//  MyCitiesTableViewCell.swift
//  weatherApp
//
//  Created by Владислав Игнатьев on 01.02.2018.
//  Copyright © 2018 Владислав Игнатьев. All rights reserved.
//

import UIKit


// MARK: - MyCitiesTableViewCell
final class MyCitiesTableViewCell: UITableViewCell, TableViewDequeuable {

  // MARK: IBOutlets
  @IBOutlet private weak var myCityNameLabel: UILabel!


  // MARK: - Overrided
  override func prepareForReuse() {
    super.prepareForReuse()
    
    setCityName(text: nil)
  }


  // MARK: - Methods
  func setCityName(text labelText: String?) {
    myCityNameLabel.text = labelText
  }
}
