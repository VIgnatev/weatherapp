//
//  MyCitiesTableViewController.swift
//  weatherApp
//
//  Created by Владислав Игнатьев on 01.02.2018.
//  Copyright © 2018 Владислав Игнатьев. All rights reserved.
//

import UIKit
import RealmSwift


// MARK: - MyCitiesTableViewController
final class MyCitiesTableViewController: UITableViewController {
  
  // MARK: Properties
  private var myCities     = [City]()
  private var selectedCity = City(name: "")

  
  // MARK: - Overrided
  override func viewDidLoad() {
    super.viewDidLoad()
    title = "My Cities"
    
    myCities = getSavedCities()
    
    tableView.reloadData()
  }
  
  // Проверка: можно ли выполнить переход
  override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
    switch identifier {
    case "toWeatherViewController":
      guard sender is City else {
        assertionFailure("Incorrect sender Type")
        return false
      }
      return true
      
    default:
      return false
    }
  }
  
  // Подготовка к переходу
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard let segueIdentifier = segue.identifier else {
      showOkAlertWith(message: "Unknown error")
      return
    }
    
    switch segueIdentifier {
    case "toWeatherViewController":
      guard let weatherViewController = segue.destination as? WeatherCollectionViewController,
        let _ = sender as? City else { // city -> _
          showOkAlertWith(message: "Unknown error")
          return
      }
      weatherViewController.cityName = selectedCity.name
      
    default: break
    }
  }
  
  
  // MARK: - Methods
  @IBAction private func addButtonPressed(_ sender: UIBarButtonItem) {
    let citiesViewController = CitiesListViewController.instantiateFromStoryboard()
    self.navigationController?.pushViewController(citiesViewController, animated: true)
    
    // Получаем город из другого контроллера
    citiesViewController.configureSelectionAction { [weak self] city in
      guard let sself = self else { return }
      
      let isAlreadyContained = sself.myCities.contains(where: { $0 == city })
      if !isAlreadyContained {
        sself.saveCity(city)
        sself.myCities.append(city)
      }
      
      DispatchQueue.main.async { // Обновляем UI
        self?.tableView?.reloadData()
      }
    }
  }
  
  // Отображение сохранённых городов
  private func getSavedCities() -> [City] {
    let realm = try! Realm()
    
    let cityEntities   = realm.objects(CityEntity.self)
    let cities: [City] = cityEntities.map { City(name: $0.name) }
    
    return cities
  }
  
  // Добавить город в БД
  private func saveCity(_ city: City) {
    let newCity  = CityEntity()
    newCity.name = city.name
    
    do {
      let realm = try! Realm()
      print(realm.configuration.fileURL!)
      
      realm.beginWrite()
      realm.add(newCity, update: true)
      
      try realm.commitWrite()
    } catch {
      print(error.localizedDescription)
    }
  }
}


// MARK: - TableViewDataSource
extension MyCitiesTableViewController {
  
  // MARK: - Overrided
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return myCities.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: MyCitiesTableViewCell = tableView.dequeue(forIndexPath: indexPath)
    
    let city = myCities[indexPath.row]
    cell.setCityName(text: city.name)
    
    return cell
  }
}


// MARK: - TableViewDelegate
extension MyCitiesTableViewController {
  
  // MARK: - Overrided
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle,
                          forRowAt indexPath: IndexPath) {
//    let cityForDeleting = myCities[indexPath.row]
//
//    if editingStyle == .delete {
//      do {
//        let realm = try! Realm()
//
//        realm.beginWrite()
//        realm.delete(cityForDeleting.name)
//        realm.delete(cityForDeleting)
//
//        try realm.commitWrite()
//      } catch {
//        print(error)
//      }
//    }
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    selectedCity = myCities[indexPath.row]
    performSegue(withIdentifier: "toWeatherViewController", sender: selectedCity)
  }
}
